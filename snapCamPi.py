import cv2
import glob
import json
import multiprocessing as mp
import os
from imutils.video import VideoStream
from nb_iot import *


def make_directory(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)
    elif os.path.exists(directory):
        for the_file in os.listdir(directory):
            file_path = os.path.join(directory, the_file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
            except Exception as e:
                print(e)


def make_buffer(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)
    elif os.path.exists(directory):
        for the_file in os.listdir(directory):
            file_path = os.path.join(directory, the_file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
            except Exception as e:
                print(e)


def open_camera():
    img_counter = 0
    using_pi_camera = True
    frame_size = (config['PICAM']['WIDTH'], config['PICAM']['HEIGHT'])
    end = False

    while True:
        cam = VideoStream(src=0, usePiCamera=using_pi_camera, resolution=frame_size, framerate=60).start()
        time.sleep(2)

        while not end:
            print('Open Camera..')
            frame = cam.read()
            # cv2.imshow('Webcam', frame)

            img_name = 'IMG%04d.jpg' % img_counter
            scale_percent = 75
            width = int(frame.shape[1] * scale_percent / 100)
            height = int(frame.shape[0] * scale_percent / 100)
            dim = (width, height)
            resize_image = cv2.resize(frame, dim, interpolation=cv2.INTER_AREA)

            cv2.imwrite(config['PATH']['WRITE_BUFFER'] + img_name, resize_image, [cv2.IMWRITE_JPEG_QUALITY,
                                                                                  config['PATH']['JPEG_QUALITY']])
            cv2.imwrite(config['PATH']['WRITE_IMG'] + img_name, frame)
            print('{} written!'.format(img_name))

            cam.stop()
            cv2.destroyAllWindows()
            end = True

        if end:
            time.sleep(config['PATH']['CAM_WAIT'])
            img_counter += 1
            end = False


def send_image():
    while True:
        time.sleep(1)
        for filename in sorted(glob.glob(config['PATH']['READ_BUFFER'])):
            with open(filename, 'rb') as file:
                content = file.read()
                print(filename)
                nb.sendImage(content)
                time.sleep(5)
            os.remove(filename)
            print('Done remove')
            print('===================================================\n')


if __name__ == '__main__':
    with open('config.json', 'r') as f:
        config = json.load(f)

    nb = NBIot()
    nb.udp_server(config['PATH']['UDP_IP'], config['PATH']['UDP_PORT'])
    nb.begin(config['PATH']['PORT'], config['PATH']['BAUDRATE'], 0)

    try:
        make_directory(config['PATH']['DIRECTORY'])
        make_buffer(config['PATH']['BUFFER'])

        p1 = mp.Process(target=open_camera)
        p1.start()

        p2 = mp.Process(target=send_image)
        p2.start()

    except KeyboardInterrupt:
        print('Measurement stopped by User')
        p1.join()
        p2.join()
        p1.terminate()
        p2.terminate()
        nb.close()
