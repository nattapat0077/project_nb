import json
import socket
import datetime
import binascii
import os
import sys

with open('server_config.json', 'r') as f:
    config = json.load(f)

UDP_IP = config['PATH']['IP']
UDP_PORT = config['PATH']['PORT']
img_counter = 0
list_result = []
list_msgID = []

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP, UDP_PORT))
print('>>>> Ready <<<<')
print('My IP :', UDP_IP, '\n', 'Port :', UDP_PORT)

directory = config['PATH']['DIRECTORY']

if not os.path.exists(directory):
    os.makedirs(directory)
elif os.path.exists(directory):
    for the_file in os.listdir(directory):
        file_path = os.path.join(directory, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            print(e)


def append_data(data_hex):
    real_data = data_hex.replace(dataHex[:12], '').replace(dataHex[-12:], '')
    print(real_data)
    list_result.append(real_data)
    list_msgID.append(dataHex[:12])
    response_data = dataHex[:12].replace('ff', '').encode()
    return response_data


while True:

    data, addr = sock.recvfrom(1024)
    dataHex = binascii.hexlify(data).decode()

    print('-----------------------------------------------------')
    print('Message received at : ' + datetime.datetime.now().strftime("%H:%M:%S.%f"))
    print('Message : ' + dataHex)

    end = False

    if dataHex[:12] != dataHex[-12:]:
        if len(list_result) == 0 or len(list_msgID) == 0:
            dataHex_rsp = append_data(dataHex)
        else:
            if dataHex[:12] == list_msgID[-1]:
                if list_msgID[-1] == 'ff30303031ff':
                    print('Reject old data in list Because found id = 0001')
                    list_result.clear()
                    list_msgID.clear()
                    dataHex_rsp = append_data(dataHex)
                else:
                    print('Same package')
                    sameData = dataHex[:12].replace(dataHex[:12], 'ff21212121ff')
                    dataHex_rsp = sameData.replace('ff', '').encode()
            elif dataHex[:12] == 'ff30303031ff':
                print('Reject old data in list Because found id = 0001')
                list_result.clear()
                list_msgID.clear()
                dataHex_rsp = append_data(dataHex)
            else:
                dataHex_rsp = append_data(dataHex)

    elif dataHex[:12] == dataHex[-12:]:
        if len(list_result) == 0 or len(list_msgID) == 0:
            print('No data in list')
            dataHex_rsp = dataHex[:12].replace('ff', '').encode()
        else:
            if dataHex[:12] == list_msgID[-1]:
                print('Same package')
                print(dataHex[:12])
                sameData = dataHex[:12].replace(dataHex[:12], 'ff21212121ff')
                print(sameData)
                dataHex_rsp = sameData.replace('ff', '').encode()
            else:
                dataHex_rsp = append_data(dataHex)
                end = True
    else:
        print('Error')
        sys.exit(1)
        # Exit the programs

    print(dataHex[:12])
    sent = sock.sendto(dataHex_rsp, addr)

    if end:
        img_name = 'IMG{}.jpg'.format(img_counter)
        s = ''
        joinData = s.join(list_result)
        # print(joinData)

        data = bytes.fromhex(joinData)

        with open(directory + img_name, 'wb') as file:
            file.write(data)

        print('Image save..')
        list_result.clear()
        list_msgID.clear()
        img_counter += 1
    elif not end:
        pass
    else:
        print('Save Error')
        sys.exit(1)
        # Exit the programs
