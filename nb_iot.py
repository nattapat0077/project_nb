import binascii
import datetime
import sys
import serial
import time

list_result = []


class NBIot:

    def __init__(self):
        self.ser = serial.Serial()
        print('================= AIS NB-IoT Shield Magellan IoT Platform =================')
        print('======================== For Send Image over NB-IoT =======================')

    def read_data_line(self):
        end = False
        rsp_AT = b''

        while not end:
            if self.ser.inWaiting() > 0:
                rsp_AT = self.ser.readline()
                if rsp_AT != b'\r\n':
                    list_result.append(rsp_AT.decode('utf-8', 'ignore').replace('\r\n', ''))
                    if rsp_AT == b'OK\r\n' or rsp_AT == b'ERROR\r\n' or rsp_AT == b'+CGATT:1\r\n':
                        end = True
            else:
                break

        return rsp_AT

    def show_data(self):
        for list in list_result[:-1]:
            print('-->', list)
        print('\n')
        del list_result[:]
        return list

    def cmd_expect_resp(self, cmd, resp, errorp, timeout, wait_text=''):
        now = time.time()
        future = now + timeout

        print_time = now
        rsp = False
        error = False
        self.ser.write(cmd)

        while True:
            data = self.read_data_line()
            # data_rsp = self.read_data_line()
            # img = data_rsp[0]

            if wait_text != '' and time.time() > print_time + 1:
                print_time = time.time()

            if data == resp:
                rsp = True
                error = False
                break
            elif data == errorp:
                rsp = False
                error = True
                break
            else:
                pass

            if time.time() > future:
                break

        return rsp, error

    def udp_server(self, udp_ip, udp_port):
        self.udpIP = udp_ip
        self.udpPort = udp_port
        print('UDP IP:', self.udpIP, '|', 'Port:', self.udpPort) # View ip&port

#Start connect NB-IoT
    def begin(self, port, baud, timeout):
        self.ser.port = port
        self.ser.baudrate = baud

        print('Port:', self.ser.port, '|', 'Baudrate:', self.ser.baudrate, '\n')  # View port&baudrate

        try:
            if not self.ser.isOpen():
                print('>>Open serial port...')
                self.ser.open()
            else:
                print('>>>Port is opened!')
        except IOError:
            print('**Port Error!')
            pass

        print('>Test AT')
        complete, Error = self.cmd_expect_resp(b'AT\r\n', b'OK\r\n', b'ERROR\r\n', 1)
        if complete:
            print('>>OK\n')
            del list_result[:]
        elif Error:
            print('>>Error AT cmd\n')
            return

        print('>>>Reboot Module')
        complete, Error = self.cmd_expect_resp(b'AT+NRB\r\n', b'OK\r\n', b'ERROR\r\n', 5)
        if complete:
            print('>>>>OK\n')
            del list_result[:]
        elif Error:
            print('>>>>Error Reboot\n')
            return
        time.sleep(5)

        print('>Get Firmware Version')
        complete, Error = self.cmd_expect_resp(b'AT+CGMR\r\n', b'OK\r\n', b'ERROR\r\n', 5)
        if complete:
            NBIot.show_data(self)
            pass
        elif Error:
            print('>>>Error get firmware version\n')
            return

        print('>Get Signal')
        complete, Error = self.cmd_expect_resp(b'AT+CSQ\r\n', b'OK\r\n', b'ERROR\r\n', 5)
        if complete:
            NBIot.show_data(self)
            pass
        elif Error:
            print(self.read_data_line())
            print('>>>Error get firmware version\n')
            return

        print('>Get IMSI')
        complete, Error = self.cmd_expect_resp(b'AT+CIMI\r\n', b'OK\r\n', b'ERROR\r\n', 5)
        if complete:
            NBIot.show_data(self)
            pass
        elif Error:
            print('>>>Error get IMSI\n')
            return

        print('>Get IMEI')
        complete, Error = self.cmd_expect_resp(b'AT+CGSN=1\r\n', b'OK\r\n', b'ERROR\r\n', 5)
        if complete:
            NBIot.show_data(self)
            pass
        elif Error:
            print('>>>Error get IMEI\n')
            return

        print('>Set Phone Functionality')
        complete, Error = self.cmd_expect_resp(b'AT+CFUN=1\r\n', b'OK\r\n', b'ERROR\r\n', 5)
        if complete:
            print('>>>OK\n')
            del list_result[:]
            pass
        elif Error:
            print('>>>Error Set phone functionality to mobule\n')
            return
        time.sleep(5)

        print('>Config network parameter')
        complete, Error = self.cmd_expect_resp(b'AT+NCONFIG=AUTOCONNECT,TRUE\r\n', b'OK\r\n', b'ERROR\r\n', 1)
        if complete:
            print('>>>OK\n')
            del list_result[:]
        elif Error:
            print('>>>Error Config parameter to module\n')
            return
        time.sleep(5)

        while True:
            attach = False
            print('>Command attach network')
            while not attach:
                complete, Error = self.cmd_expect_resp(b'AT+CGATT=1\r\n', b'OK\r\n', b'ERROR\r\n', 1)
                if complete:
                    print('>>>OK\n')
                    attach = True
                elif Error:
                    print('>>>Error can not attach network\n')
                    attach = False

            while attach:
                time.sleep(5)
                print('>>Connecting')
                complete, Error = self.cmd_expect_resp(b'AT+CGATT?\r\n', b'+CGATT:1\r\n', b'+CGATT:0\r\n', 1, '.')
                if complete:
                    print('>>>Done\n')
                    break
                elif Error:
                    print('>>>Error Timeout attach network')
                    attach = False

            if attach:
                break
            else:
                print('>>>>Restart attach!\n')

        time.sleep(5)
        print('Test Ping')
        AT_Ping = 'AT+NPING=' + str(self.udpIP) + '\r\n'
        AT_Ping_msg = AT_Ping.encode()
        complete, Error = self.cmd_expect_resp(AT_Ping_msg, b'OK\r\n', b'ERROR\r\n', 5)
        if complete:
            print('IP :',self.udpIP)
            print('>>>Pass\n')
            del list_result[:]
        elif Error:
            print('>>>Error test ping\n')
            return
        time.sleep(5)

        print('Create socket')
        AT_Cs = 'AT+NSOCR=DGRAM,17,' + str(self.udpPort) + ',1\r\n'
        AT_Cs_msg = AT_Cs.encode()
        complete, Error = self.cmd_expect_resp(AT_Cs_msg, b'OK\r\n', b'ERROR\r\n', 5)
        if complete:
            print('>>>Ready for send Image')
            del list_result[:]
        elif Error:
            print('>>>Error can not create socket')
            return
        time.sleep(5)

    def sendImage(self, data):
        dataHex = binascii.hexlify(data).decode()
        length = int(len(dataHex)/2)
        lengthOfdata = str(length).encode('utf-8').hex() # length Of img in HEX format
        
        msg_id = 1
        splitData = self.splitCount(dataHex, 512) # Split 512characters(256Bytes) in dataHex
        print('Split Messages!!\n')
        print('>List Messages before split', len(splitData))

        for dataList in splitData:
            print('Send Messages at', msg_id)
            msg_idf = '%04d' % (msg_id) #msg_id with format
            length_splitDataf = '%04d' % (len(splitData)) #length_splitData with format
            packData = 'FF' + msg_idf.encode('utf-8').hex() + 'FF' + str(dataList) + 'FF' + \
                        length_splitDataf.encode('utf-8').hex() + 'FF'
            AT_Sent = 'AT+NSOST=0,' + str(self.udpIP) + ',' + str(self.udpPort) + ',' + \
                        str(int(len(packData) / 2)) + ',' + packData + '\r\n'
            AT_Sent_msg = AT_Sent.encode()
            print(AT_Sent_msg) #Debug Message
            time.sleep(2)
            self.sendAndres(AT_Sent_msg, msg_idf.encode('utf-8').hex())
            msg_id += 1

    def sendAndres(self, AT_Sent_msg, msg_idf):
        previous = time.time()
        now = time.time()
        print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        rsp_AT = b''
        end = False
        flag_rcv = False
        flag_retransmit = False
        flag_send = False
        wait_time = [2, 4, 8, 16, 32, 64]

        self.ser.flushInput()

        for i in range(0, 6):
            if flag_rcv:
                break
            else:
                if flag_send:
                    flag_retransmit = True

            if flag_retransmit:
                self.ser.write(AT_Sent_msg)
                # pass
            else:
                self.ser.write(AT_Sent_msg)
                flag_send = True

            while True:
                if self.ser.inWaiting() > 0:
                    char = self.ser.read()
                    if char == b'\r' or char == b'\n':
                        end = True
                    else:
                        rsp_AT = rsp_AT + char

                if end:
                    str_rsp_AT = rsp_AT.decode("utf-8")
                    if '0,' + self.udpIP in str_rsp_AT:
                        rsp_payload = str_rsp_AT.split(',')
                        # print('response payload ', rsp_payload) #Debug some error!!
                        if rsp_payload[5] == '0':
                            flag_rcv = self.unpack(rsp_payload[4], msg_idf)
                        else: # > 0
                            flag_rcv = self.unpack(rsp_payload[4], msg_idf)
                            print('rsp_payload[5] > 0')
                            self.ser.write(b'AT+NSORF=0,512\r\n')
                            # sys.exit(1)
                    elif 'OK' in str_rsp_AT:
                        if time.time() > (previous + 0.5):
                            self.ser.write(b'AT+NSORF=0,512\r\n')
                        else:
                            continue
                    elif '+NSONMI' in str_rsp_AT:
                        rsp_payloadRF = str_rsp_AT.split(',')
                        if rsp_payloadRF[1] <= '512':
                            rsp_payloadRFCommand = 'AT+NSORF=0,' + rsp_payloadRF[1] + '\r\n'
                            self.ser.write(rsp_payloadRFCommand.encode())
                        else:
                            self.ser.write(b'AT+NSORF=0,512\r\n')

                    end = False
                    rsp_AT = b''

                    if time.time() > (now + wait_time[i]):
                        now = time.time()
                        if wait_time[i] == 2:
                            print('.')
                        elif wait_time[i] == 4:
                            print('..')
                        elif wait_time[i] == 8:
                            print('...')
                        elif wait_time[i] == 16:
                            print('....')
                        elif wait_time[i] == 32:
                            print('.....')
                        elif wait_time[i] == 64:
                            print('......')
                        break
                    elif flag_rcv:
                        print('---------------------- END ------------------------')
                        break

        if not flag_rcv:
            print('Timeout')
            print('---------------------- END ------------------------')
        # return

    def unpack(self, rspdata, msg_idf):
        rspdata_Decode = binascii.unhexlify(rspdata).decode('utf-8')
        print('\n== Response from server ==')
        # print('>', rspdata_Decode)
        # print('>>', binascii.unhexlify(rspdata_Decode).decode('utf-8'))

        if rspdata_Decode == msg_idf:
            print('OK!!')
            rcv_rsp = True
        elif rspdata_Decode == "21212121":
            print('Reject Message!')
            rcv_rsp = True
        else:
            print('Not Match!')
            rcv_rsp = False

        # rcv_rsp = True
        return rcv_rsp

    def splitCount(self, s, count):
        return [s[i:i + count] for i in range(0, len(s), count)]

    def close(self):
        self.ser.close()
