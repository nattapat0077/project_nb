import cv2
import time

cam = cv2.VideoCapture(0)
previous = time.time()
delta = 0
img_counter = 0
print('Open Camera..')

while True:
    current = time.time()
    delta += current - previous
    previous = current

    ret, frame = cam.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cv2.imshow('webcam', frame)

    img_name = 'IMG%04d.jpg' % img_counter
    scale_percent = 75  # percent of original size
    width = int(frame.shape[1] * scale_percent / 100)
    height = int(frame.shape[0] * scale_percent / 100)
    dim = (width, height)
    resize_image = cv2.resize(frame, dim, interpolation=cv2.INTER_AREA)
    resize_imageG = cv2.resize(gray, dim, interpolation=cv2.INTER_AREA)

    if delta > 1:
        cv2.imwrite('../project_NB/test/' + img_name, resize_image, [cv2.IMWRITE_JPEG_QUALITY, 40])

        # cv2.imwrite('../project_nb/test/img/100' + img_name, resize_image, [cv2.IMWRITE_JPEG_QUALITY, 100])
        # cv2.imwrite('../project_nb/test/img_gray/100' + img_name, resize_imageG, [cv2.IMWRITE_JPEG_QUALITY, 100])
        #
        # cv2.imwrite('../project_nb/test/img/75' + img_name, resize_image, [cv2.IMWRITE_JPEG_QUALITY, 75])
        # cv2.imwrite('../project_nb/test/img_gray/75' + img_name, resize_imageG, [cv2.IMWRITE_JPEG_QUALITY, 75])
        #
        # cv2.imwrite('../project_nb/test/img/50' + img_name, resize_image, [cv2.IMWRITE_JPEG_QUALITY, 50])
        # cv2.imwrite('../project_nb/test/img_gray/50' + img_name, resize_imageG, [cv2.IMWRITE_JPEG_QUALITY, 50])
        #
        # cv2.imwrite('../project_nb/test/img/25' + img_name, resize_image, [cv2.IMWRITE_JPEG_QUALITY, 25])
        # cv2.imwrite('../project_nb/test/img_gray/25' + img_name, resize_imageG, [cv2.IMWRITE_JPEG_QUALITY, 25])

        print('{} written!'.format(img_name))
        img_counter += 1
        delta = 0
    if img_counter == 100:
        break

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cam.release()
cv2.destroyAllWindows()
