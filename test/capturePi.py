import time
import cv2
from imutils.video import VideoStream

usingPiCamera = True
frameSize = (640, 512)
cam = VideoStream(src=0, usePiCamera=usingPiCamera, resolution=frameSize, framerate=60).start()
time.sleep(2.0)
previous = time.time()
delta = 0
img_counter = 0
print('Open Camera..')

while True:
    current = time.time()
    delta += current - previous
    previous = current

    frame = cam.read()
    cv2.imshow('Webcam', frame)

    img_name = 'IMG%04d.jpg' % img_counter
    scale_percent = 75  # percent of original size
    width = int(frame.shape[1] * scale_percent / 100)
    height = int(frame.shape[0] * scale_percent / 100)
    dim = (width, height)
    resize_image = cv2.resize(frame, dim, interpolation=cv2.INTER_AREA)

    if delta > 1:
        cv2.imwrite('../project_nb/test/' + img_name, resize_image, [cv2.IMWRITE_JPEG_QUALITY, 40])
        cv2.imwrite('../project_nb/test2/' + img_name, resize_image)
        print('{} written!'.format(img_name))
        img_counter += 1
        delta = 0

    if img_counter == 100:
        break

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cam.stop()
cv2.destroyAllWindows()
