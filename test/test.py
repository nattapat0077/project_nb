import cv2
import time

cam = cv2.VideoCapture(0)
previous = time.time()
delta = 0
first_img = 0
img_counter = 0
print('Open Camera..')

while True:
    current = time.time()
    delta += current - previous
    previous = current

    ret, frame = cam.read()
    cv2.imshow('Webcam', frame)

    img_name = 'IMG%04d.jpg' % (img_counter)
    scale_percent = 75  # percent of original size
    width = int(frame.shape[1] * scale_percent / 100)
    height = int(frame.shape[0] * scale_percent / 100)
    dim = (width, height)
    resized = cv2.resize(frame, dim, interpolation=cv2.INTER_AREA)

    if first_img == 0:
        cv2.imwrite('../project_NB/test/' + img_name, resized, [cv2.IMWRITE_JPEG_QUALITY, 40])
        print('{} written!'.format(img_name))
        img_counter += 1
        first_img = 1

    if delta > 5:
        cv2.imwrite('../project_NB/test/' + img_name, resized, [cv2.IMWRITE_JPEG_QUALITY, 40])
        print('{} written!'.format(img_name))
        img_counter += 1
        delta = 0

    if img_counter == 10:
        break

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cam.release()
cv2.destroyAllWindows()
