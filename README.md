###Hardware :
1. Raspberry Pi3
2. Webcam or Raspberry Module
3. Adapter for Raspberry
4. USB to serial(CP2104)
5. AIS NB-IoT
6. Wired 5 pin

###Software :
1. Raspbian OS
2. Python 3.5.x or Better
3. OpenCV 3.4.5
    
###Server(Azure) :
1. Ubuntu Server 16.04.1
2. [Install XFCE4 on Ubuntu Server](https://docs.microsoft.com/en-us/azure/virtual-machines/linux/use-remote-desktop)
```bash
sudo apt-get update
sudo apt-get install xfce4

sudo apt-get install xrdp
sudo systemctl enable xrdp

echo xfce4-session >~/.xsession

sudo service xrdp restart
```
3. Install Nomacs
```bash
sudo apt-get install nomacs
```

###How to wired between USB to serial to AIS NB-IoT
![NB-IoT connect](/image/01_nb_connect.png)
    
###Explain file and directory
- Server/ directory is for server
- test/ directory is for benchmark
- file
    - config.json --> for config programs
    - nb_iot.py --> AIS NB-IoT library
    - run.py --> Turn on the camera and use the SPACE BAR to take pictures
    - autoCapture.py --> Turn on the camera and take a picture at the set time
    - snapCam.py --> Turn the camera on and take a picture. When finished, the camera will turn off. And when the time is set, the camera will open again.
    - faceDetect.py --> Turn on the camera and use the face detection system and take pictures.
    - xxxxPi.py --> for Raspberry Pi camera module
      
###How run this code
- On Raspberry
    - Open Terminal
    - Go to path
    - "source ~/.profile"
    - "workon cv"
    - "python3 filename.py"
    ![open_opencv](/image/04_open_opencv.jpg)
- On server
    - Open Terminal
    - Go to path
    - Type "python3 udp_server.py" or "python3 udp_server_oop.py"